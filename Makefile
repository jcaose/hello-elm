
.PHONY: clean build deploy

forceapp_package_path = force-app/main/default
all:
	@echo nothing

build:
	rm dist/*
	elm make src/Main.elm --optimize --output dist/elm.js
	uglifyjs dist/elm.js --compress "pure_funcs=[F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9],pure_getters,keep_fargs=false,unsafe_comps,unsafe" \
		| uglifyjs --mangle --output dist/elm.min.js

deploy:
	zip -r  $(forceapp_package_path)/staticresources/app.zip ./dist
	mv  $(forceapp_package_path)/staticresources/app.zip  $(forceapp_package_path)/staticresources/jcapp1.resource
	rm -rf ./build/sfdc-package
	sfdx force:source:convert  -d ./build/sfdc-package
	#  sfdx force:auth:web:login -sr https://sinch2--cpqpoc.my.salesforce.com/ -a sinch2cpq
	sfdx force:mdapi:deploy -u sinch2cpq -d ./build/sfdc-package -w 10000


