```
 > elm make src/HomePage.elm --output elm.js
```

## Elm Live

```
$ npm install --global elm elm-live@next
$ elm-live src/Main.elm --open --start-page=custom.html -- --output=main.js

```



## Optimize
```
elm make src/Main.elm --optimize --output=elm.js
```



## SFDX
```
sfdx force:auth:web:login -sr https://sinch2--cpqpoc.my.salesforce.com/ -a sinch2cpq

```


## Use lighting in Visualforce


- https://developer.salesforce.com/docs/atlas.en-us.pages.meta/pages/pages_styling_slds.htm
- https://www.lightningdesignsystem.com/platforms/visualforce/


## Other

- https://github.com/trailheadapps/ecars



## side navbar

https://dev.to/fayaz/making-a-navigation-drawer-sliding-sidebar-with-tailwindcss-blueprint-581l

## Re Task

https://ohanhi.com/tasks-in-modern-elm.html


```
getEventsFromPastHour : Cmd Msg
getEventsFromPastHour =
    Time.now
        |> Task.andThen
            (\currentTime ->
                getEventsFrom (currentTime - Time.hour)
            )
        |> Task.attempt GotResult


getEventsFrom : Time -> Task Http.Error (List Event)
getEventsFrom time =
    Http.get (apiUrl ++ "?from=" ++ toString time) eventsListDecoder
        |> Http.toTask

```


## CSS theme

https://d-board.mobifica.com/
