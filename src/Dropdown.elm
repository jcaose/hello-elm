module Dropdown exposing (..)

import Browser.Events as Events
import Html exposing (..)
import Html.Attributes
import Html.Events exposing (onClick, onMouseEnter, onMouseLeave)
import Json.Decode as Decode

-- TOOD, different placement of popup

{-| Navbar pulldown helper
    1. add model and init from the parent
    2. pass down update messages and subscriptions
    3. add focus related events to the container
    4  add clicked events to the header
    5. add conditional open to the dropdown based model.dropdown.open
-}
type alias Model =
    { open : Bool
    , focused : Bool
    }


init : Model
init =
    { open = False
    , focused = False
    }


type Msg
    = Clicked
    | ClickedOutside
    | OutFocus
    | InFocus


update : Msg -> Model -> Model
update msg model =
    case msg of
        Clicked ->
            { model | open = not model.open }

        OutFocus ->
            { model | focused = False }

        InFocus ->
            { model | focused = True }

        ClickedOutside ->
            { model | open = False }


subscriptions : Model -> Sub Msg
subscriptions model =
    if model.focused then
        Sub.none

    else
        Events.onClick (Decode.succeed ClickedOutside)


addFocusEvents : (Msg -> a) -> List (Html.Attribute a) -> List (Html.Attribute a)
addFocusEvents msg attrs =
    List.concat
        [ attrs
        , [ onMouseLeave OutFocus
          , onMouseEnter InFocus
          ] |> List.map (Html.Attributes.map msg)
        ]

addClickEvent: (Msg->a) -> List (Html.Attribute a) -> List (Html.Attribute a)
addClickEvent msg attrs =
    (onClick Clicked |> Html.Attributes.map msg ) :: attrs
--stopPropagationOn "click" (Decode.succeed ( Clicked, True )