module Icons exposing (..)

import Html exposing (Html, i)
import Html.Attributes exposing (attribute)
import Svg exposing (..)
import Svg.Attributes exposing (..)



outlined  :  String -> Html a
outlined name =
    i [class "material-icons-outlined  md-dark"] [text name ]


outlinedsm  :  String -> Html a
outlinedsm name =
    i [class "material-icons-outlined md-18"] [text name ]

outlinedxs:  String -> Html a
outlinedxs name =
    -- 1rem 16px
    i [class "material-icons-outlined text-base"] [text name ]

xmlns : Html.Attribute a
xmlns =
    attribute "xmlns" "http://www.w3.org/2000/svg"


menuburger : Html a
menuburger =
    svg
        [ stroke "currentColor"
        , fill "none"
        , strokeWidth "2"
        , viewBox "0 0 24 24"
        , strokeLinecap "round"
        , strokeLinejoin "round"
        , height "20"
        , width "20"
        , xmlns
        ]
        [ line [ x1 "3", y1 "12", x2 "21", y2 "12" ] []
        , line [ x1 "3", y1 "6", x2 "21", y2 "6" ] []
        , line [ x1 "3", y1 "18", x2 "21", y2 "18" ] []
        ]


search : Html a
search =
    svg
        [ stroke "currentColor"
        , fill "none"
        , strokeWidth "2"
        , viewBox "0 0 24 24"
        , strokeLinecap "round"
        , strokeLinejoin "round"
        , class "stroke-current h-4 w-4"
        , height "1em"
        , width "1em"
        , xmlns
        ]
        [ circle [ cx "11", cy "11", r "8" ] []
        , line [ x1 "21", y1 "21", x2 "16.65", y2 "16.65" ] []
        ]


angleSmallDown : Html a
angleSmallDown =
    svg
        [ class "stroke-current"
        , fill "none"
        , attribute "height" "1em"
        , attribute "stroke" "currentColor"
        , attribute "stroke-linecap" "round"
        , attribute "stroke-linejoin" "round"
        , attribute "stroke-width" "2"
        , viewBox "0 0 24 24"
        , attribute "width" "1em"
        , xmlns
        ]
        [ node "polyline" [ attribute "points" "6 9 12 15 18 9" ] []
        ]


cube : Html a
cube =
    svg [ fill "none", attribute "height" "18", attribute "stroke" "currentColor", attribute "stroke-linecap" "round", attribute "stroke-linejoin" "round", attribute "stroke-width" "2", viewBox "0 0 24 24", attribute "width" "18", attribute "xmlns" "http://www.w3.org/2000/svg" ]
        [ Svg.path [ d "M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z" ]
            []
        , node "polyline"
            [ attribute "points" "3.27 6.96 12 12.01 20.73 6.96" ]
            []
        , node "line"
            [ attribute "x1" "12", attribute "x2" "12", attribute "y1" "22.08", attribute "y2" "12" ]
            []
        ]


home : Html a
home =
    svg [ class "stroke-current text-xl text-gray-700", fill "none", attribute "height" "1em", attribute "stroke" "currentColor", attribute "stroke-linecap" "round", attribute "stroke-linejoin" "round", attribute "stroke-width" "2", viewBox "0 0 24 24", attribute "width" "1em", attribute "xmlns" "http://www.w3.org/2000/svg" ]
        [ Svg.path [ d "M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z" ]
            []
        , node "polyline"
            [ attribute "points" "9 22 9 12 15 12 15 22" ]
            []
        ]


account : Html a
account =
    svg [ class "stroke-current text-xl text-blue-700", fill "none", attribute "height" "1em", attribute "stroke" "currentColor", attribute "stroke-linecap" "round", attribute "stroke-linejoin" "round", attribute "stroke-width" "2", viewBox "0 0 24 24", attribute "width" "1em", attribute "xmlns" "http://www.w3.org/2000/svg" ]
        [ Svg.path [ d "M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2" ]
            []
        , node "circle"
            [ attribute "cx" "12", attribute "cy" "7", attribute "r" "4" ]
            []
        ]


comment : Html a
comment =
    svg [ class "stroke-current text-xl text-orange-500", fill "none", attribute "height" "1em", attribute "stroke" "currentColor", attribute "stroke-linecap" "round", attribute "stroke-linejoin" "round", attribute "stroke-width" "2", viewBox "0 0 24 24", attribute "width" "1em", attribute "xmlns" "http://www.w3.org/2000/svg" ]
        [ Svg.path [ d "M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z" ]
            []
        ]


settings : Html a
settings =
    svg [ class "stroke-current text-xl text-green-700", fill "none", attribute "height" "1em", attribute "stroke" "currentColor", attribute "stroke-linecap" "round", attribute "stroke-linejoin" "round", attribute "stroke-width" "2", viewBox "0 0 24 24", attribute "width" "1em", attribute "xmlns" "http://www.w3.org/2000/svg" ]
        [ node "circle"
            [ attribute "cx" "12", attribute "cy" "12", attribute "r" "3" ]
            []
        , Svg.path [ d "M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z" ]
            []
        ]


maps : Html a
maps =
    svg [ class "stroke-current text-xl text-red-500", fill "none", attribute "height" "1em", attribute "stroke" "currentColor", attribute "stroke-linecap" "round", attribute "stroke-linejoin" "round", attribute "stroke-width" "2", viewBox "0 0 24 24", attribute "width" "1em", attribute "xmlns" "http://www.w3.org/2000/svg" ]
        [ Svg.path [ d "M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z" ]
            []
        , node "circle"
            [ attribute "cx" "12", attribute "cy" "10", attribute "r" "3" ]
            []
        ]


twitter : Html a
twitter =
    svg [ class "stroke-current text-xl text-twitter", fill "none", attribute "height" "1em", attribute "stroke" "currentColor", attribute "stroke-linecap" "round", attribute "stroke-linejoin" "round", attribute "stroke-width" "2", viewBox "0 0 24 24", attribute "width" "1em", attribute "xmlns" "http://www.w3.org/2000/svg" ]
        [ Svg.path [ d "M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z" ]
            []
        ]


facebook : Html a
facebook =
    svg [ class "stroke-current text-xl text-facebook", fill "none", attribute "height" "1em", attribute "stroke" "currentColor", attribute "stroke-linecap" "round", attribute "stroke-linejoin" "round", attribute "stroke-width" "2", viewBox "0 0 24 24", attribute "width" "1em", attribute "xmlns" "http://www.w3.org/2000/svg" ]
        [ Svg.path [ d "M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z" ]
            []
        ]


instagram : Html a
instagram =
    svg [ class "stroke-current text-xl text-instagram", fill "none", attribute "height" "1em", attribute "stroke" "currentColor", attribute "stroke-linecap" "round", attribute "stroke-linejoin" "round", attribute "stroke-width" "2", viewBox "0 0 24 24", attribute "width" "1em", attribute "xmlns" "http://www.w3.org/2000/svg" ]
        [ node "rect"
            [ attribute "height" "20", attribute "rx" "5", attribute "ry" "5", attribute "width" "20", attribute "x" "2", attribute "y" "2" ]
            []
        , Svg.path [ d "M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z" ]
            []
        , node "line"
            [ attribute "x1" "17.5", attribute "x2" "17.51", attribute "y1" "6.5", attribute "y2" "6.5" ]
            []
        ]


linkedin : Html a
linkedin =
    svg [ class "stroke-current text-xl text-linkedin", fill "none", attribute "height" "1em", attribute "stroke" "currentColor", attribute "stroke-linecap" "round", attribute "stroke-linejoin" "round", attribute "stroke-width" "2", viewBox "0 0 24 24", attribute "width" "1em", attribute "xmlns" "http://www.w3.org/2000/svg" ]
        [ Svg.path [ d "M16 8a6 6 0 0 1 6 6v7h-4v-7a2 2 0 0 0-2-2 2 2 0 0 0-2 2v7h-4v-7a6 6 0 0 1 6-6z" ]
            []
        , node "rect"
            [ attribute "height" "12", attribute "width" "4", attribute "x" "2", attribute "y" "9" ]
            []
        , node "circle"
            [ attribute "cx" "4", attribute "cy" "4", attribute "r" "2" ]
            []
        ]

message : Html a
message =
    svg [ fill "none", attribute "height" "18", attribute "stroke" "currentColor", attribute "stroke-linecap" "round", attribute "stroke-linejoin" "round", attribute "stroke-width" "2", viewBox "0 0 24 24", attribute "width" "18", attribute "xmlns" "http://www.w3.org/2000/svg" ]
            [ Svg.path [ d "M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z" ]
                []
            ]


dboardlogo : Html a
dboardlogo =
        svg [ fill "none", attribute "height" "28", attribute "stroke" "currentColor", attribute "stroke-linecap" "round", attribute "stroke-linejoin" "round", attribute "stroke-width" "2", viewBox "0 0 24 24", attribute "width" "28", attribute "xmlns" "http://www.w3.org/2000/svg" ]
        [ Svg.path [ d "M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z" ]
            []
        , node "polyline" [ attribute "points" "3.27 6.96 12 12.01 20.73 6.96" ]
            []
        , node "line" [ attribute "x1" "12", attribute "x2" "12", attribute "y1" "22.08", attribute "y2" "12" ]
            []
        ]
