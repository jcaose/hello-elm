module Leftsidebar exposing (..)

import Array exposing(Array)
import Asset
import Html exposing (..)
import Html.Attributes exposing (..)
import Leftsidebar.Menuitem as Menuitem


type alias Model =
    { sections : Array ( String, Menuitem.Model )
    }


init : Model
init =
    { sections = Array.fromList <|
        [ ( "dashboard"
          , Menuitem.Model
                { title = "Dashboard"
                , active = False
                , link = "/#"
                , open = False
                , siblings = Array.empty
                }
          )
        , ( "hiking"
          , Menuitem.Model
                { title = "Hiking"
                , active = False
                , link = ""
                , open = True 
                , siblings =
                    Array.fromList <|
                        [ leafitem "Social feed" "#/social"
                        , leafitem "Tasks" "#/tasks"
                        , leafitem "Inbox" "#/inbox"
                        , leafitem "Todo" "#/todo"
                        ]
                }
          )
        ]
    }


leafitem : String -> String -> Menuitem.Model
leafitem title link =
    Menuitem.Model
        { title = title
        , active = False
        , link = link
        , open = False
        , siblings = Array.empty
        }


type Msg
    = GotMenuitemMsg Int Menuitem.Msg


update : Msg -> Model -> Model
update msg model =
    case msg of 
        GotMenuitemMsg idx  subMsg -> 
            case Array.get idx model.sections of 
                Just (icon, item) ->
                    { model | sections = Array.set idx (icon,  Menuitem.update subMsg item) model.sections}
                _ -> model 


view : Model -> Html Msg
view model =
    div [ class "left-sidebar left-sidebar-1" ]
        [ div [ class "logo truncate" ]
            [ div [ class "flex flex-row items-center justify-start space-x-2" ]
                [ img [ class "w-8", Asset.src (Asset.image "sinch-logo.svg") ] []
                , span [ class "px-2" ]
                    [ text "Sinch" ]
                ]

            -- TODO close here
            ]
        , div [ class "left-sidebar-title" ]
            [ span [] [ text "Applications" ]
            ]
        , ul []  
             (model.sections |> Array.toIndexedList |> List.map viewSection)
        ]

viewSection: (Int, (String, Menuitem.Model)) -> Html Msg
viewSection (idx, (icon, model)) = 
     Menuitem.view icon model  |> Html.map (GotMenuitemMsg idx )


