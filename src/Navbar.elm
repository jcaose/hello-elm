module Navbar exposing (..)

import String
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)

import Dropdown as Dropdown
import Icons
-- import Session


type alias Model =
    { exploremenu : Dropdown.Model
    , countrysel : Dropdown.Model
    , appsel : Dropdown.Model
    , status : Dropdown.Model
    , notification : Dropdown.Model
    , avatar : Dropdown.Model
    , appList : List ( String, String ) 
    }


init : Model
init =
    { exploremenu = Dropdown.init
    , countrysel = Dropdown.init
    , appsel = Dropdown.init
    , status = Dropdown.init
    , notification = Dropdown.init
    , avatar = Dropdown.init
    , appList =
        [ ( "home", "Home" )
        , ( "account_circle", "Account" )
        , ( "chat_bubble_outline", "Comment" )
        , ( "settings", "Settings" )
        , ( "place", "Maps" )
        ]
    }


type Msg
    = LeftsidebarToggled
    | GotExploremenuMsg Dropdown.Msg
    | GotCountryselMsg Dropdown.Msg
    | GotAppselMsg Dropdown.Msg
    | GotStatusMsg Dropdown.Msg
    | GotNotificationMsg Dropdown.Msg
    | GotAvatarMsg Dropdown.Msg


update : Msg -> Model -> Model
update msg model =
    case msg of
        LeftsidebarToggled  -> model

        GotExploremenuMsg m -> 
           { model | exploremenu = Dropdown.update m model.exploremenu} 


        GotCountryselMsg m ->
            { model | countrysel = Dropdown.update m model.countrysel }
            

        GotAppselMsg m ->
            { model | appsel = Dropdown.update m model.appsel }

        GotStatusMsg m ->
            { model | status = Dropdown.update m model.status }

        GotNotificationMsg m ->
            { model | notification = Dropdown.update m model.notification }

        GotAvatarMsg m ->
            { model | avatar = Dropdown.update m model.avatar }


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Dropdown.subscriptions model.exploremenu |> Sub.map GotExploremenuMsg
        , Dropdown.subscriptions model.countrysel |> Sub.map GotCountryselMsg
        , Dropdown.subscriptions model.appsel|> Sub.map GotAppselMsg
        , Dropdown.subscriptions model.status|> Sub.map GotStatusMsg
        , Dropdown.subscriptions model.notification|> Sub.map GotNotificationMsg
        , Dropdown.subscriptions model.avatar |> Sub.map GotAvatarMsg
        ]


view : Model -> Html Msg
view model =
    div
        [ class "navbar navbar-1 border-b" ]
        [ div [ class "navbar-inner w-full flex items-center justify-start" ]
            [ viewMenuburger
            , viewSearchbox
            , viewExploreMenu model.exploremenu
            , span [ class "ml-auto" ] []
            , viewCountrysel model.countrysel
            , viewAppsel model.appsel model.appList
            , viewStatus model.status 
            , viewNotification model.notification
            , viewAvatar model.avatar
            , viewSettings
            ]
        ]
viewMenuburger : Html Msg 
viewMenuburger = 
    button [ class "mx-4", onClick LeftsidebarToggled]
        [
            Icons.outlined "menu"
        ]
viewSearchbox : Html Msg 
viewSearchbox = 
    Html.form [ class "w-full max-w-xs mr-2 navbar-search" ]
        [ div [ class "relative" ]
            [ input
                [ type_ "search"
                , name "search"
                , placeholder "Search..."
                , class
                    "pl-10 pr-5 appearance-none h-10 w-full rounded text-sm focus:outline-none"
                ]
                []
            , button [ type_ "submit", class "absolute top-0 mt-3 left-0 ml-4" ]
                [ Icons.search ]
            ]
        ]

viewExploreMenu : Dropdown.Model -> Html Msg
viewExploreMenu dropdown = 
    div ([class "relative"]
            |> Dropdown.addFocusEvents GotExploremenuMsg)
        [ button
            ([ class "btn btn-default btn-icon bg-transparent h-16" ]
                |> Dropdown.addClickEvent GotExploremenuMsg
            )
            [ span [ class "mr-2" ] [ text "Explore" ]
            , Icons.angleSmallDown
            ]
        , div
            [ class "dropdown absolute top-0 left-0 mt-16 "
            , classList [ ( "open", dropdown.open ) ]
            ]
            [ div [ class "dropdown-content w-128 bottom-start" ]
                [ div [ class "flex flex-row flex-wrap" ]
                    [ sampleExploreText 1
                    , sampleExploreText 2
                    , sampleExploreText 3
                    , sampleExploreText 4
                    ]
                ]
            ]
        ]
sampleExploreText : Int -> Html msg
sampleExploreText n =
    div [ class "w-1/2 p-2 dropdown-item" ]
        [ div [ class "flex flex-row items-center justify-start" ]
            [ div [ class "flex-shrink-0 w-8" ]
                [ span [ class "h-8 w-8 bg-blue-500 text-white flex items-center justify-center rounded-full text-lg font-display font-bold" ]
                    [ text ("0" ++ String.fromInt n ) ]
                ]
            , div [ class "ml-2" ]
                [ div [ class "text-sm font-bold" ] [ text "Teams" ]
                , div [ class "text-xs" ] [ text "nima quasi sunt" ]
                ]
            ]
        ] 

viewCountrysel: Dropdown.Model -> Html Msg
viewCountrysel dropdown = 
    div
        ([ class "hidden lg:flex relative" ]
            |> Dropdown.addFocusEvents GotCountryselMsg
        )
        [ button
            ([ class "flex items-center justify-center h-16 w-12"
            ]|> Dropdown.addClickEvent GotCountryselMsg)

            [ span [ class "text-base flag-icon flag-icon-se" ] []
            ]
        , div
            [ class "dropdown absolute top-0 right-0 mt-16 "
            , classList [ ( "open", dropdown.open ) ]
            ]
            [ div [ class "dropdown-content w-64 bottom-start" ]
                [ div [ class "dropdown-title" ] [ text "Change country" ]
                , div [ class "flex flex-wrap" ]
                    [ viewCountryItem
                    , viewCountryItem
                    , viewCountryItem
                    ]
                ]
            ]
        ]



viewCountryItem : Html a
viewCountryItem =
    div
        [ class "w-1/2 flex items-center justify-start p-2 text-sm space-x-2 dropdown-item"
        ]
        [ span [ class "text-base flag-icon flag-icon-se" ] []
        , span [] [ text "Sweden" ]
        ]


viewAppsel : Dropdown.Model -> List (String, String) -> Html Msg
viewAppsel dropdown appList = 
    div
        ([ class "hidden lg:flex relative"
        ] |> Dropdown.addFocusEvents GotAppselMsg)
        [ button
            ([ class "flex items-center justify-center h-16 w-12"
            ] |> Dropdown.addClickEvent GotAppselMsg)
            [ Icons.outlined "apps"

            -- Icons.cube
            ]
        , div
            [ class "dropdown absolute top-0 right-0 mt-16 "
            , classList [ ( "open", dropdown.open ) ]
            ]
            [ div [ class "dropdown-content w-64 bottom-start" ]
                [ div [ class "dropdown-title" ]
                    [ text "Apps" ]
                , div [ class "flex flex-wrap text-center" ] <|
                    List.map viewApp appList
                ]
            ]
        ]


viewApp : ( String, String ) -> Html a
viewApp ( icon, name ) =
    div [ class "w-1/3 flex flex-col items-center justify-center h-20 space-y-1 dropdown-item" ]
        [ span [ class "material-icons-outlined" ] [ text icon ]
        , span [ class "text-xs" ] [ text name ]
        ]

viewStatus : Dropdown.Model -> Html Msg
viewStatus dropdown = 
    div ([ class "hidden lg:flex relative"
        ] |> Dropdown.addFocusEvents GotStatusMsg)
         [ button
            ([ class "flex items-center justify-center h-16 w-12 relative"
             ] |> Dropdown.addClickEvent GotStatusMsg)
            [ --- Icons.message
                Icons.outlined "chat_bubble_outline"
              -- TODO move this to  model
            , span [ class "absolute uppercase font-bold inline-flex text-center p-0 leading-none text-2xs h-4 w-4 inline-flex items-center justify-center rounded-full bg-blue-500 text-white", attribute "style" "top:14px;right:8px" ]
                [ text "5" ]
            ]
        , div
            [ class "dropdown absolute top-0 right-0 mt-16"
            , classList [ ( "open", dropdown.open ) ]
            ]
            [ div [ class "dropdown-content w-64 bottom-start" ]
                [ div [ class "dropdown-title" ]
                    [ text "Project status" ]
                , div [ class "flex flex-col" ]
                    [ div [ class "flex flex-col p-2 dropdown-item" ]
                        [ div [ class "flex items-center justify-between mb-2" ]
                            [ div [ class "text-sm font-bold" ]
                                [ text "Mobile app development" ]
                            , div [ class "text-xs whitespace-nowrap" ]
                                [ text "33"
                                , text "%"
                                ]
                            ]
                        , div [ class "relative flex flex-row w-full text-center text-xs items-center h-1" ]
                            [ div [ class "top-0 left-0 h-1 w-full bg-green-500", attribute "style" "width:33%" ]
                                []
                            ]
                        ]
                    , div [ class "flex flex-col p-2 dropdown-item" ]
                        [ div [ class "flex items-center justify-between mb-2" ]
                            [ div [ class "text-sm font-bold" ]
                                [ text "Deploy github project" ]
                            , div [ class "text-xs whitespace-nowrap" ]
                                [ text "50"
                                , text "%"
                                ]
                            ]
                        , div [ class "relative flex flex-row w-full text-center text-xs items-center h-1" ]
                            [ div [ class "top-0 left-0 h-1 w-full bg-yellow-500", attribute "style" "width:50%" ]
                                []
                            ]
                        ]
                    , div [ class "flex flex-col p-2 dropdown-item" ]
                        [ div [ class "flex items-center justify-between mb-2" ]
                            [ div [ class "text-sm font-bold" ]
                                [ text "Customer development" ]
                            , div [ class "text-xs whitespace-nowrap" ]
                                [ text "66"
                                , text "%"
                                ]
                            ]
                        , div [ class "relative flex flex-row w-full text-center text-xs items-center h-1" ]
                            [ div [ class "top-0 left-0 h-1 w-full bg-red-500", attribute "style" "width:66%" ]
                                []
                            ]
                        ]
                    , div [ class "flex flex-col p-2 dropdown-item" ]
                        [ div [ class "flex items-center justify-between mb-2" ]
                            [ div [ class "text-sm font-bold" ]
                                [ text "Database backup" ]
                            , div [ class "text-xs whitespace-nowrap" ]
                                [ text "25"
                                , text "%"
                                ]
                            ]
                        , div [ class "relative flex flex-row w-full text-center text-xs items-center h-1" ]
                            [ div [ class "top-0 left-0 h-1 w-full bg-indigo-500", attribute "style" "width:25%" ]
                                []
                            ]
                        ]
                    , div [ class "flex flex-col p-2 dropdown-item" ]
                        [ div [ class "flex items-center justify-between mb-2" ]
                            [ div [ class "text-sm font-bold" ]
                                [ text "Release version 1.4" ]
                            , div [ class "text-xs whitespace-nowrap" ]
                                [ text "80"
                                , text "%"
                                ]
                            ]
                        , div [ class "relative flex flex-row w-full text-center text-xs items-center h-1" ]
                            [ div [ class "top-0 left-0 h-1 w-full bg-blue-500", attribute "style" "width:80%" ]
                                []
                            ]
                        ]
                    ]
                ]
            ]
        ]


viewNotification : Dropdown.Model -> Html Msg
viewNotification dropdown = 
    div ([ class "hidden lg:flex relative" ]|> Dropdown.addFocusEvents GotNotificationMsg)
        [ button
            ([ class "flex items-center justify-center h-16 w-12" ]
             |> Dropdown.addClickEvent GotNotificationMsg)
            [ Icons.outlined "notifications"
            ]
        , div
            [ class "dropdown absolute top-0 right-0 mt-16"
            , classList [ ( "open", dropdown.open ) ]
            ]
            [ div [ class "dropdown-content w-64 bottom-start" ]
                [ div [ class "flex flex-row flex-wrap" ]
                    [ div [ class "w-full" ]
                        [ div [ class "flex items-center justify-start dropdown-item p-2" ]
                            [ div [ class "ml-2" ]
                                [ div [ class "text-sm font-bold" ]
                                    [ text "5 new sales" ]
                                , div [ class "text-xs" ]
                                    [ text "Atque quaerat libero maiores vel." ]
                                ]
                            ]
                        ]
                    , div [ class "w-full" ]
                        [ div [ class "flex items-center justify-start dropdown-item p-2" ]
                            [ div [ class "ml-2" ]
                                [ div [ class "text-sm font-bold" ]
                                    [ text "$3.000 in average profits" ]
                                , div [ class "text-xs" ]
                                    [ text "Aut aut ullam eum possimus." ]
                                ]
                            ]
                        ]
                    , div [ class "w-full" ]
                        [ div [ class "flex items-center justify-start dropdown-item p-2" ]
                            [ div [ class "ml-2" ]
                                [ div [ class "text-sm font-bold" ]
                                    [ text "200 new tweets" ]
                                , div [ class "text-xs" ]
                                    [ text "Fugiat praesentium soluta amet non." ]
                                ]
                            ]
                        ]
                    , div [ class "w-full" ]
                        [ div [ class "flex items-center justify-start dropdown-item p-2" ]
                            [ div [ class "ml-2" ]
                                [ div [ class "text-sm font-bold" ]
                                    [ text "2 new items" ]
                                , div [ class "text-xs" ]
                                    [ text "Fugit eaque molestias et aut." ]
                                ]
                            ]
                        ]
                    , div [ class "w-full" ]
                        [ div [ class "flex items-center justify-start dropdown-item p-2" ]
                            [ div [ class "ml-2" ]
                                [ div [ class "text-sm font-bold" ]
                                    [ text "51 registered users" ]
                                , div [ class "text-xs" ]
                                    [ text "Labore aut voluptas molestias illo." ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]

viewAvatar : Dropdown.Model -> Html Msg
viewAvatar dropdown =
    div
        ([ class "relative" ] |>Dropdown.addFocusEvents GotAvatarMsg)
        [ button
            ([ class "flex h-16 w-8 rounded-full ml-2 relative" ]|> Dropdown.addClickEvent GotAvatarMsg)
            [ span [ class "absolute top-0 left-0 pt-4" ]
                [ img [ alt "avatar", class "h-8 w-8 rounded-full shadow", src "https://lh3.googleusercontent.com/ogw/ADea4I4336_z6Rs2DL_W2WEp1wvFEGK7ACRkZxT7GbqIxw=s83-c-mo" ]
                    []
                , span [ class "absolute uppercase font-bold inline-flex text-center p-0 leading-none text-2xs h-4 w-4 inline-flex items-center justify-center rounded-full bg-red-500 text-white ring-white", attribute "style" "top:10px;right:-4px" ]
                    [ text "2" ]
                ]
            ]
        , div
            [ class "x dropdown absolute top-0 right-0 mt-16 "
            , classList [ ( "open", dropdown.open ) ]
            ]
            [ div [ class "dropdown-content w-48 bottom-end" ]
                [ div [ class "flex flex-col w-full" ]
                    [ ul [ class "list-none" ]
                        [ li [ class "dropdown-item" ]
                            [ a [ class "flex flex-row items-center justify-start h-10 w-full px-2", href "/" ]
                                [ Icons.outlinedsm "mail"
                                , span [ class "mx-2" ]
                                    [ text "Inbox" ]
                                , span [ class "uppercase font-bold inline-flex text-center p-0 leading-none text-2xs h-4 w-4 inline-flex items-center justify-center rounded-full bg-red-500 text-white ml-auto" ]
                                    [ text "2" ]
                                ]
                            ]
                        , li [ class "dropdown-item" ]
                            [ a [ class "flex flex-row items-center justify-start h-10 w-full px-2", href "/" ]
                                [Icons.outlinedsm "star_border"
                                , span [ class "mx-2" ]
                                    [ text "Messages" ]
                                , span [ class "uppercase font-bold inline-flex text-center p-0 leading-none text-2xs h-4 w-4 inline-flex items-center justify-center rounded-full bg-blue-500 text-white ml-auto" ]
                                    [ text "3" ]
                                ]
                            ]
                        , li [ class "dropdown-item" ]
                            [ a [ class "flex flex-row items-center justify-start h-10 w-full px-2", href "/extras/user-profile" ]
                                [ Icons.outlinedsm "person_outline"
                                , span [ class "mx-2" ]
                                    [ text "Profile" ]
                                ]
                            ]
                        , li [ class "dropdown-item" ]
                            [ a [ class "flex flex-row items-center justify-start h-10 w-full px-2", href "/pages/logout" ]
                                [ Icons.outlinedsm "logout"
                                , span [ class "mx-2" ]
                                    [ text "Logout" ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]



viewSettings : Html Msg
viewSettings =
    button [ class "btn-transparent flex items-center justify-center h-16 w-8 mx-4" ]
    [ Icons.outlined "settings" ]