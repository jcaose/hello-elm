module Route exposing (..)

import Browser.Navigation as Nav
import Html.Attributes as Attr
import Html exposing (Attribute)
import Url exposing (Url)
import Url.Parser as Parser exposing ((</>), Parser, oneOf, s)


type Route
    = Home
    | Root
    | Login
    | Logout


parser : Parser (Route -> a) a
parser = 
    oneOf
        [ Parser.map Home Parser.top
        , Parser.map  Login (s "login")
        , Parser.map Logout (s "logout")]
        

href : Route -> Attribute msg 
href targetRoute = 
    Attr.href (routeToString targetRoute)
    
replaceUrl : Nav.Key -> Route -> Cmd msg
replaceUrl key route = 
    Nav.replaceUrl key (routeToString route)


fromUrl : Url -> Maybe Route
fromUrl url = 
    -- fragment like a path ? RealWorld spec
    {url | path = Maybe.withDefault "" url.fragment, fragment = Nothing}
        |> Parser.parse parser 
--- INTERNAL

routeToString : Route -> String
routeToString page = 
    "#/" ++ String.join "/" (routeToPieces page)

routeToPieces : Route -> List String
routeToPieces page = 
    case page of 
        Home -> []
        Root -> []
        Login -> ["login"]
        Logout -> ["logout"]
