module Main exposing (main)

import Html exposing (..)

import Browser exposing (Document)
import Browser.Navigation as Nav
import Url exposing (Url)
import Json.Decode as Decode exposing (Value)

import Api
import Page
import Page.Home as Home
import Page.Blank as Blank
import Page.Login as Login
import Page.NotFound as NotFound
import Route
import Maybe exposing (withDefault)

import Session exposing (Session)

-- Ref "Realworld Elm"


type Model
    = Redirect Session
    | NotFound Session
    | Home Home.Model
    | Login Login.Model

-- The first argument is not the type of the flag Value
-- We need to anther layer to decode it and set the default if missing
-- Session already include the Nav.Key in the
init : Session.Zession -> Url -> Nav.Key  ->( Model, Cmd Msg )
init session url navKey  =
    changeRouteTo (Route.fromUrl url)
        (Redirect (Session.fromSession navKey session))


view : Model -> Document Msg
view model =
    let
        user =
            (Session.zessionFrom (toSession model)).user

        viewPage page toMsg config =
            let
                { title, body } =
                    Page.view user page config
            in
            { title = title
            , body = List.map (Html.map toMsg) body
            }
    in
    case model of
        Redirect _ ->
            Page.view user Page.Other Blank.view

        NotFound _ ->
            Page.view user Page.Other NotFound.view

        Home home ->
            viewPage Page.Home GotHomeMsg (Home.view home)

        Login login ->
            viewPage Page.Other GotLoginMsg (Login.view login)


type Msg
    = ChangedUrl Url
    | ClickedLink Browser.UrlRequest
    | GotHomeMsg Home.Msg
    | GotLoginMsg Login.Msg
    | GotSession Session


toSession : Model -> Session
toSession page =
    case page of
        Redirect session ->
            session

        NotFound session ->
            session

        Home home ->
            Home.toSession home

        Login login ->
            Login.toSession login


changeRouteTo : Maybe Route.Route -> Model -> ( Model, Cmd Msg )
changeRouteTo maybeRoute model =
    let
        session = toSession model
    in
    case maybeRoute of
        Nothing ->
            ( NotFound session, Cmd.none )

        Just Route.Root ->
            ( model, Route.replaceUrl (Session.navKey session) Route.Home )

        Just Route.Logout ->
            (model, Api.logout)

        Just Route.Login ->
            Login.init session
                |> updateWith Login GotLoginMsg model

        Just Route.Home ->
            Home.init session
                |> updateWith Home GotHomeMsg model



update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model ) of
        ( ClickedLink urlRequest, _ ) ->
            case urlRequest of
                Browser.Internal url ->
                    case url.fragment of
                        Nothing ->
                            -- handle the case from href ""
                            -- unncessary if path based routing as oppose to
                            -- the frragment based routing
                            ( model, Cmd.none )

                        Just _ ->
                            ( model
                            , Nav.pushUrl (Session.navKey (toSession model)) (Url.toString url)
                            )

                Browser.External href ->
                    ( model, Nav.load href )

        (GotLoginMsg subMsg, Login login ) ->
            Login.update subMsg login
                |> updateWith Login GotLoginMsg model

        ( ChangedUrl url, _) ->
            changeRouteTo (Route.fromUrl url) model

        (GotHomeMsg subMsg, Home home) ->
            Home.update subMsg home |> updateWith Home GotHomeMsg model

        (_, _) ->
            -- discarg all other messages
            ( model, Cmd.none)

-- The 3rd argument is  the  model at the highest level
updateWith : (subModel-> Model ) -> (subMsg -> Msg) ->  Model -> (subModel, Cmd subMsg) -> (Model, Cmd Msg)
updateWith toModel toMsg _ (subModel, subCmd) =
    ( toModel subModel
    , Cmd.map toMsg subCmd
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        NotFound _ ->
            Sub.none
        Redirect _ ->
            Session.changes GotSession (Session.navKey (toSession model ))

        Login login ->
            Sub.map GotLoginMsg (Login.subscriptions login)

        Home home ->
            Sub.map GotHomeMsg (Home.subscriptions home)

main : Program Value Model Msg
main =
    let
        init2 flags url navKey  =
            let
               maybeSession =
                    Decode.decodeValue Decode.string flags
                        |> Result.andThen (Decode.decodeString Session.decoder)
                        |> Result.toMaybe
            in
                init (withDefault Session.default  maybeSession) url  navKey

    in
    Browser.application
        { init = init2
        , onUrlChange = ChangedUrl
        , onUrlRequest = ClickedLink
        , subscriptions = subscriptions
        , update = update
        , view = view
        }
