module Skeleton exposing (..)

-- Page Skeleton

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Attributes.Aria exposing (..)
import Html.Events exposing (onClick)
import Icons
import Json.Encode as Json
import Leftsidebar
import Navbar


type alias Model =
    { navbar : Navbar.Model
    , leftsidebarCollapsed : Bool
    , leftsidebar : Leftsidebar.Model
    , altertbarHidden : Bool
    }


init : Model
init =
    { navbar = Navbar.init
    , leftsidebarCollapsed = False
    , leftsidebar = Leftsidebar.init
    , altertbarHidden = False
    }


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Navbar.subscriptions model.navbar |> Sub.map GotNavbarMsg
        ]


type Msg
    = GotNavbarMsg Navbar.Msg
    | GotleftsidebarMsg Leftsidebar.Msg
    | AlertbarToggled


toggleLeftsidebar : Navbar.Msg -> Bool -> Bool
toggleLeftsidebar msg collapsed =
    case msg of
        Navbar.LeftsidebarToggled ->
            not collapsed

        _ ->
            collapsed


update : Msg -> Model -> Model
update msg model =
    case msg of
        GotNavbarMsg m ->
            { model
                | navbar = Navbar.update m model.navbar
                , leftsidebarCollapsed = toggleLeftsidebar m model.leftsidebarCollapsed
            }

        AlertbarToggled ->
            { model | altertbarHidden = not model.altertbarHidden }
            
        GotleftsidebarMsg m -> 
            { model | leftsidebar = Leftsidebar.update m model.leftsidebar}



-- VIEW


viewRightSidebar : Model -> Html msg
viewRightSidebar _ =
    div [] []

--- sinde this is skeleton we cannot use our own msg 
view : Model -> (Msg -> msg) -> Html msg -> Html msg  
view model gotMsg page =
    div []
        [ div [ id "portal" ]
            [ if not model.altertbarHidden then
                viewAlertbar |> Html.map gotMsg
              else
                div [] []
            ]
        , div
            [ dataLayout "layout-1"
            , dataCollapsed model.leftsidebarCollapsed
            , dataBackground "light"
            , dataNavbar "light"
            , dataLeftsidebar "light"
            , dataRightsidebar "light"
            , class "font-sans antialiased text-sm disable-scrollbars"
            ]
            [ viewRightSidebar model
            , div [ class "wrapper" ]
                [Leftsidebar.view model.leftsidebar |> Html.map GotleftsidebarMsg |> Html.map gotMsg
                , div
                    [ class "main w-full bg-gray-50 text-gray-900 dark:bg-gray-9000 dark:text-white" ]
                    [ Navbar.view model.navbar |> Html.map GotNavbarMsg |> Html.map gotMsg
                    , div [ class "min-h-screen w-full p-4" ]
                        [ -- page content goes here
                          page
                        ]
                    ]
                ]
            ]
        ]



viewAlertbar : Html Msg
viewAlertbar =
    div [ class "z-50 h-auto w-full p-0 block" ]
        [ div [ class "w-full flex items-center justify-start p-4 bg-blue-500 text-white" ]
            [ div [ class "mr-2" ]
                [ Icons.outlinedxs "info"
                ]
            , div [ class "flex-grow" ]
                [ span []
                    [ text "This is an important message!" ]
                ]
            , div [ class "flex-shrink" ]
                [ button [ class "ml-auto flex items-center justify-center", onClick AlertbarToggled ]
                    [ Icons.outlinedxs "close"
                    ]
                ]
            ]
        ]


dataAttr : String -> String -> Attribute msg
dataAttr name value =
    attribute ("data-" ++ name) value


dataLayout : String -> Attribute msg
dataLayout value =
    dataAttr "layout" value


dataCollapsed : Bool -> Attribute msg
dataCollapsed value =
    dataAttr "collapsed" (Json.encode 0 (Json.bool value))


dataBackground : String -> Attribute msg
dataBackground value =
    dataAttr "background" value


dataNavbar : String -> Attribute msg
dataNavbar value =
    dataAttr "navbar" value


dataLeftsidebar : String -> Attribute msg
dataLeftsidebar value =
    dataAttr "left-sidebar" value


dataRightsidebar : String -> Attribute msg
dataRightsidebar value =
    dataAttr "right-sidebar" value
