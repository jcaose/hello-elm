module Leftsidebar.Menuitem exposing (..)

import Array exposing (Array)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Icons

type Model
    = Model
        { title : String
        , active : Bool

        -- link mean leaf node
        , link : String

        --  node that contains children
        , open : Bool
        , siblings : Array Model
        }


init_ : String -> String -> List Model -> Model
init_ title link siblings =
    Model
        { title = title
        , active = False
        , link = link
        , open = False
        , siblings = Array.fromList siblings
        }


type Msg
    = Clicked
      -- originated from n:th sibling  
    | GotSiblingMsg Int Msg

update : Msg -> Model -> Model 
update msg (Model model) = 
    case msg of
        Clicked -> 
            Model {model | open = not model.open }
        GotSiblingMsg n subMsg  ->
            case  Array.get n model.siblings of 
                Nothing -> Model model 
                Just item ->
                    Model { model | siblings = Array.set n (update subMsg item) model.siblings}

view : String -> Model -> Html Msg 
view icon (Model model) = 
    if Array.isEmpty model.siblings then 
        -- 
         li [ class "l0" ]
            [ a 
                [ class "left-sidebar-item"
                , classList [ ("active", model.active && model.open)
                            , ("open-sibling", model.open )
                            , ("hidden-sibling", not model.open)
                            ] 
                , href (model.link)
                ]
                [ Icons.outlined icon
                , span [ class "title" ] [ text model.title ]
                ]
            ]
    else 
        -- more complicate case 
        li [ class "l0" ]
            -- TODO: add case when level 0 contains the link 
            [ button 
                [ class "left-sidebar-item"
                , classList [ ("active", model.active && model.open)
                            , ("open-sibling", model.open )
                            , ("hidden-sibling", not model.open)
                            ] 
                , onClick Clicked
                ]
                [ Icons.outlined icon
                , span [ class "title" ] [ text model.title ]
                , div [ class "ml-auto arrow mr-4" ] [ Icons.outlinedxs "chevron_right" ]
                ]
            , ul [] (  model.siblings  |> Array.indexedMap (viewItem 1 )|> Array.toList )
            ]




viewItem : Int-> Int -> Model -> Html Msg 
viewItem level idx (Model model) = 
    (if Array.isEmpty model.siblings then 
        -- case: leaf node 
        li [ class ("l" ++ (String.fromInt level)) ]
        [ a
            [ class "left-sidebar-item"
            , classList [ ( "active", model.active) ]
            , href model.link
            ] 
            [ span [ class "title" ] [ text model.title ] 
            ]
        ]
    else 
        -- case: with children 
        li [ class ("l"++ (String.fromInt level)) ]
        [ button
            [ class "left-sidebar-item"
            , classList[ ("active", model.active && model.open)
                       , ("open-sibling", model.open)
                       , ("hidden-sibling", not model.open)
                       ] 
            , onClick Clicked 
            ]
            [ span [ class "title" ] [ text model.title ]
            -- CSS hack, JC I have to manuall add some right padding 
            -- not sure why we don't need it in the original d-board
            , div [ class "ml-auto arrow mr-4" ] [ Icons.outlinedxs "chevron_right" ]
            ]
        , ul [] (  model.siblings  |> Array.indexedMap (viewItem (level + 1) )|> Array.toList )
        ]
    ) |> Html.map (GotSiblingMsg idx)


