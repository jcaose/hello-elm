module User exposing (..)


import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode 

import Json.Encode exposing (Value)

import Json.Decode.Pipeline exposing (required)

import User.Username as Username




type alias User = 
    { username : Username.Username
    , token :  String 
    }


-- Quick refesher
--  https://guide.elm-lang.org/effects/json.html
--  https://package.elm-lang.org/packages/NoRedInk/elm-json-decode-pipeline/latest

decoder : Decoder User
decoder =
    Decode.succeed User
        |> required "username" Username.decoder
        |> required "token" Decode.string



encode : User -> Value
encode  user  = 
    Encode.object
        [ ( "username", Username.encode user.username)
        , ( "token", Encode.string user.token )
        ]
