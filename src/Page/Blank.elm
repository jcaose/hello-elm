module Page.Blank exposing (..)


import Html exposing (..)


view : { title : String, content : Html msg }
view =
    { title = ""
    , content = text ""
    }