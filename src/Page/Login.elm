module Page.Login exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onSubmit)
import Http
import Route
import Session exposing (Session)
import Task
import Html.Events exposing (onClick)


import User.Username as Username
import User exposing (User)


type alias Model =
    { session : Session
    , problems : List Problem
    , form : Form
    }


type Problem
    = InvalidEntry ValidatedField String
    | ServerError String


type alias Form =
    { email : String
    , password : String
    }


init : Session -> ( Model, Cmd msg )
init session =
    ( { session = session
      , problems = []
      , form =
            { email = ""
            , password = ""
            }
      }
    , Cmd.none
    )


view : Model -> { title : String, content : Html Msg }
view _ =
    { title = "Login"
    , content =
        div
            [ attribute "data_layout" "centered"
            , class "w-full h-screen flex items-center justify-center bg-gray-50"
            ]
            [ div [ class "flex flex-col bg-white border border-gray-200 p-8 w-full max-w-xl" ]
                [ div [ class "flex flex-col w-full mb-4" ]
                    [ div [ class "text-xs uppercase" ]
                        [ text "Login"
                        ]
                    , div [ class "text-lg font-bold" ]
                        [ text "Please enter your username and password to login"
                        ]
                    ]
                , div [ class "flex flex-col" ]
                    [ Html.form
                        [ class "form flex flex-wrap w-full"
                        , onSubmit SubmittedForm
                        ]
                        [ div [ class "w-full" ]
                            [ div [ class "form-element" ]
                                [ div [ class "form-label" ]
                                    [ text "Email" ]
                                , input [ class "form-input ", name "email", placeholder "Enter you email", type_ "email" ]
                                    []
                                ]
                            , div [ class "form-element" ]
                                [ div [ class "form-label" ]
                                    [ text "Password" ]
                                , input [ class "form-input ", name "password", placeholder "Enter your password", type_ "password" ]
                                    []
                                ]
                            ]
                        , Html.input [ type_ "submit", class "btn btn-default bg-blue-500 hover:bg-blue-600 text-white btn-rounded" ] []
                        ]
                    ]
                , div [ class "w-full mt-3 mb-6" ]
                    [ text "signup with"
                    ]
                , div [ class "flex flex-row w-full" ]
                    [ text "new user"
                    ]
                , div [ class "w-full" ]
                    [ text "forgot password"
                    ]
                , div [ class "btn btn-default bg-red-500", onClick PearlyGatesPass] 
                    [
                        text "Pearly Gates is open on staging"
                    ]
                ]
            ]
    }



-- TODO viewProblem


type Msg
    = SubmittedForm
    | EnteredEmail String
    | EnteredPassword String
    | CompletedLogin (Result Http.Error User)
    | GotSession Session
    | PearlyGatesPass


fakeuser : User 
fakeuser =
    { username = Username.Username "fakeuser"
    , token = "faketoken"
    }


type TrimmedForm
    = Trimmed Form


type ValidatedField
    = Email
    | Password


fieldsToValidate : List ValidatedField
fieldsToValidate =
    [ Email
    , Password
    ]


trimFields : Form -> TrimmedForm
trimFields form =
    Trimmed
        { email = String.trim form.email
        , password = String.trim form.password
        }


validateField : TrimmedForm -> ValidatedField -> List Problem
validateField (Trimmed form) field =
    List.map (InvalidEntry field) <|
        case field of
            Email ->
                if String.isEmpty form.email then
                    [ "email can't be blank." ]

                else
                    []

            Password ->
                if String.isEmpty form.password then
                    [ "password can't be blank." ]

                else
                    []


validate2 : Form -> Result (List Problem) TrimmedForm
validate2 form =
    let
        trimmedForm =
            trimFields form
    in
    case List.concatMap (validateField trimmedForm) fieldsToValidate of
        [] ->
            Ok trimmedForm

        problems ->
            Err problems

-- TODO: remove hack 
validate : Form -> Result error TrimmedForm
validate form =
    Ok (trimFields form)


updateForm : (Form -> Form) -> Model -> ( Model, Cmd Msg )
updateForm transform model =
    ( { model | form = transform model.form }, Cmd.none )


fakeLoginSuccess : Cmd Msg
fakeLoginSuccess =
    Task.succeed (Ok fakeuser) |> Task.perform CompletedLogin


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SubmittedForm ->
            case validate model.form of
                Ok _ ->
                    ( { model | problems = [] }
                      --- Send httpreqest
                      --, Cmd.map (CompletedLogin (Ok  fakeuser))
                    , fakeLoginSuccess
                    )

                Err problems ->
                    ( { model | problems = problems }
                    , Cmd.none
                    )

        EnteredEmail email ->
            updateForm (\form -> { form | email = email }) model

        EnteredPassword password ->
            updateForm (\form -> { form | password = password }) model

        CompletedLogin (Err _) ->
            -- TODO handle the error
            ( model, Cmd.none )

        CompletedLogin (Ok user) ->
            let
                zession = Session.zessionFrom model.session 
            in 
            ( model, Session.store { zession | user = Just user  } )

        GotSession session ->
            ( { model | session = session }
            , Route.replaceUrl (Session.navKey session) Route.Home
            )
            
        PearlyGatesPass -> 
            (model, fakeLoginSuccess)



subscriptions : Model -> Sub Msg
subscriptions model =
    Session.changes GotSession (Session.navKey model.session)


toSession : Model -> Session
toSession model =
    model.session
