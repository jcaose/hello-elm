module Page.Home exposing (..)

-- import Html.Events exposing (onClick)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Attributes.Aria exposing (..)
import Session exposing (Session)
import Skeleton


type alias Model =
    { session : Session 
    , skeleton: Skeleton.Model
    }

init : Session -> ( Model, Cmd Msg )
init session  =
    ( { session = session
      , skeleton = Skeleton.init
      }
    , Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Skeleton.subscriptions model.skeleton |> Sub.map GotSkeletonMsg
         , Session.changes GotSession (Session.navKey model.session)
        ]


type Msg
    =  GotSkeletonMsg Skeleton.Msg
    | GotSession Session  
        


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotSkeletonMsg m ->
            ( { model | skeleton = Skeleton.update m model.skeleton }
            , Cmd.none )

        GotSession session ->
            ( { model | session = session }, Cmd.none) 



view : Model -> { title : String, content : Html Msg }
view model =
    { title = "JC DEMO HOME"
    , content =  viewHome model  
                |> Skeleton.view model.skeleton GotSkeletonMsg
    }


viewHome: Model -> Html Msg
viewHome _ =
    div [ ]
        [ text "Home Page here" 
        ]



toSession: Model -> Session 
toSession model =
    model.session
    


