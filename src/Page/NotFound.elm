module Page.NotFound exposing (..)

import Asset
import Html exposing (..)
import Html.Attributes exposing (..)



-- VIEW


view : { title : String, content : Html msg }
view =
    { title = "Page Not Found"
    , content = content
    }


content : Html msg
content =
    div [ class "w-full h-screen flex items-center justify-center bg-gray-50", attribute "data-layout" "centered" ]
        [ div [ class "flex flex-col w-full max-w-xl text-center" ]
            [ img [ alt "svg", class "object-contain w-auto h-64 mb-8", Asset.src (Asset.image "notfound.svg") ] []
            , h1 [ class "text-6xl text-blue-500 mb-4" ]
                [ text "404" ]
            , div [ class "mb-8 text-center text-gray-900" ]
                [ text "We're sorry. The page you requested could not be found. Please go back to the homepage or contact us" ]
            , div [ class "flex w-full" ]
                -- TODO : use Navigation.back()
                [ a [ class "btn btn-lg btn-rounded btn-block bg-blue-500 hover:bg-blue-600 text-white", href "/#" ]
                    [ text "Go back" ]
                ]
            ]
        ]
