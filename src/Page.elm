module Page exposing (..)


import Browser exposing (Document)

import Html exposing (..)
import Html.Attributes exposing (..)

import User exposing (User)


type Page
    = Other
    | Home
    | Login


view : Maybe User -> Page -> { title : String, content : Html msg} -> Document msg
view maybeUser page {title, content} = 
    {
        title = title ++ " - JCTEST"
    ,   body = [(viewHeader page maybeUser),  content, viewFooter]
    }


-- navbar 
viewHeader : Page -> Maybe User-> Html  msg
viewHeader _  _ = 
    nav [] []


viewFooter : Html msg
viewFooter = footer [] []