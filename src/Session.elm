module Session exposing (..)

import Api
import User exposing (User)
import Browser.Navigation as Nav
import Json.Encode as Encode exposing (Value)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)
import Maybe exposing (withDefault)


type Session = 
    Session Nav.Key Zession 

-- Zession is the session without the nav key 
type alias Zession = 
    { user : Maybe User 
    }
default :  Zession
default  = 
    { user =  Nothing
    }

zessionFrom :  Session -> Zession 
zessionFrom (Session _ s) = s

fromSession: Nav.Key -> Zession -> Session
fromSession key session = Session key session 

navKey : Session -> Nav.Key
navKey (Session key _ ) =  key 


changes : (Session -> msg) -> Nav.Key -> Sub msg
changes toMsg key = 
    sessionChanges (\session -> toMsg (fromSession key session))


sessionChanges : (Zession -> msg)  -> Sub msg
sessionChanges valueToMsg =
    Api.onStoreChange (\value -> valueToMsg (decodeFromChange value))


decodeFromChange :  Value -> Zession  
decodeFromChange val =
        -- It's stored in localStorage as a JSON String;
        -- first decode the Value as a String, then
        -- decode that String as JSON.
        Decode.decodeValue decoder val |> Result.toMaybe |> withDefault  default
    
   
encode : Zession -> Value 
encode zession = 
    let
        maybeUserEncoder = 
            case zession.user of 
                Just  u -> User.encode u 
                Nothing -> Encode.null 
    in
    Encode.object
        [
          ("user", maybeUserEncoder) 
        ]
        
maybeUserDecoder : Decoder (Maybe User)
maybeUserDecoder = Decode.nullable  User.decoder

decoder : Decoder Zession 
decoder = 
    Decode.succeed Zession  
        |> required "user"  maybeUserDecoder

store: Zession -> Cmd msg 
store zession = 
    Api.storeCache <| Just <| encode zession