port module Api exposing (..)

import Json.Decode exposing (Value)

port onStoreChange : (Value -> msg) -> Sub msg



-- TODO: only clear viewer part
logout : Cmd msg
logout =
    storeCache Nothing
    
-- save value to the local storage
port storeCache : Maybe Value -> Cmd msg

