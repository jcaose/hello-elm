#!/bin/sh

#  Scaffold the SF DX project that contains the visualforce page that loads the elm application
#  Notes:
#    - We still to deploy the static resource that contains `dist/elm.min.js` into staticresouces
#

# standard SF DX project structure
# https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_source_file_format.htm
# force app  directory structure
#  <package-dir>/force-app/main/default


RESOURCE_NAME="jcapp1"
RESOURCE_DESCR="jc elm app"
APEX_PAGE_LABEL="jc-elm"


# SF apex page meta
# Doc: https://developer.salesforce.com/docs/atlas.en-us.api_meta.meta/api_meta/meta_pages.htm
#   availableInTouch : indicate if the page can be used in the mobile  app
#   confirmationTokenRequired: Whether GET request for the page rquire a CSRF confirmation token
#   label: required label for the page
read -r -d '' SF_APEX_PAGE_META_TMPL << END
<?xml version="1.0" encoding="UTF-8"?>
<ApexPage xmlns="http://soap.sforce.com/2006/04/metadata">
    <apiVersion>52.0</apiVersion>
    <availableInTouch>false</availableInTouch>
    <confirmationTokenRequired>false</confirmationTokenRequired>
    <label>LABEL</label>
</ApexPage>
END

echo "${SF_APEX_PAGE_META_TMPL}" | \
    m4 -DLABEL="${APEX_PAGE_LABEL}"


# Note:  Need to escape $
read -r -d '' SF_APEX_PAGE_TMPL << END
<apex:page showHeader="false"
    sidebar="false"
    standardStylesheets="false"
    docType="html-5.0"
    applyBodyTag="False"
    applyHtmlTag="False"
    >
    <!--
       https://developer.salesforce.com/docs/atlas.en-us.pages.meta/pages/pages_compref_page.htm
       https://developer.salesforce.com/docs/atlas.en-us.pages.meta/pages/pages_styling_slds.htm
    -->
    <script type="text/javascript">
        //rest details
        const __ACCESSTOKEN__ = '{!\$Api.Session_ID}';
        const __RESTHOST__ = '';
    </script>

<head>
    <title>Simple Order Form App</title>
    <apex:slds />
</head>

<body class="slds-scope">
    <div id="elm-app"></div>
    <script type='text/javascript' src="{!URLFOR(\$Resource.RESOURCE_NAME, 'dist/elm.min.js')}"></script>
       <script>
        var app = Elm.Main.init({
            node: document.getElementById("elm-app")
        });
    </script>

</body>

</apex:page>
END

echo "${SF_APEX_PAGE_TMPL}" | \
    m4 -DRESOURCE_NAME="${RESOURCE_NAME}"



# SF static resource meta file
# Notes about read
#   -r not allow backslash to escape
#   -d delim continue rather than new line
read -r -d '' SF_RESOURCE_META_TMPL << END
<?xml version="1.0" encoding="UTF-8"?>
<StaticResource xmlns="http://soap.sforce.com/2006/04/metadata">
    <cacheControl>Public</cacheControl>
    <contentType>application/zip</contentType>
    <description>SF_RESOURCE_DESCR</description>
</StaticResource>
END

echo "${SF_RESOURCE_META_TMPL}" | \
    m4 -DSF_RESOURCE_DESCR="${RESOURCE_DESCR}"


## CREATE one-off sf proejct that containst the package


sfdx force:project:create --projectname project1  --defaultpackagedir jcapp1   --template empty
