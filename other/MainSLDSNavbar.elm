module MainSLDSNavbar exposing (main)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Attributes.Aria exposing (..)
import Html.Events exposing (onClick)
import Header exposing (..)


import Bootstrap.CDN as CDN
import Bootstrap.Grid as Grid


main : Program () Model Msg
main =
    Browser.sandbox { init = init, update = update, view = view }


type alias Model =
    Int


init : Model
init =
    10



-- UPDATE


type Msg
    = Increment
    | Decrement
    | Reset


update : Msg -> Model -> Model
update msg model =
    case msg of
        Increment ->
            model + 1

        Decrement ->
            model - 1

        Reset ->
            0



-- VIEW


viewAppLauncher : Model -> Html Msg
viewAppLauncher _ =
    div [ class "slds-context-bar__item slds-context-bar__dropdown-trigger slds-dropdown-trigger slds-dropdown-trigger_click slds-no-hover" ]
        [ div [ class "slds-context-bar__icon-action" ]
            [ button [ class "slds-button slds-icon-waffle_container slds-context-bar__button", title "Description of the icon when needed" ]
                [ span [ class "slds-icon-waffle" ]
                    [ span [ class "slds-r1" ] []
                    , span [ class "slds-r2" ] []
                    , span [ class "slds-r3" ] []
                    , span [ class "slds-r4" ] []
                    , span [ class "slds-r5" ] []
                    , span [ class "slds-r6" ] []
                    , span [ class "slds-r7" ] []
                    , span [ class "slds-r8" ] []
                    , span [ class "slds-r9" ] []
                    ]
                ]
            ]
        ]


viewNavbar : Model -> Html Msg
viewNavbar model =
    div [ class "slds-context-bar" ]
        [ div [ class "slds-context-bar__primary" ]
            [
                viewAppLauncher model
            ]
        , div [ class "slds-context-bar__secondary" ]
            []
        ]


view2 : Model -> Html Msg
view2 model =
    div
        [ style "height" "100%"
        ]
        --  style="background-color:#fafaf9;position:relative;height:600px;overflow:hidden;display:flex;flex-direction:"
        [ viewHeader
        , viewNavbar model
        , div
            [ style "height" "100%"

            -- , style "background-color" "#fafaf9"
            , style "position" "relative"
            , style "overflow" "hidden"
            , style "display" "flex"
            , style "flex-direction" ""
            ]
            [ viewLeftSidePane
            , div [ class "slds-grid slds-gutters" ]
                [ div [ class "slds-col slds-size_1-of-1" ]
                    [ text "blah blah"
                    ]
                , div [ class "slds-col slds-size_1-of-1" ]
                    [ button [ class "slds-button slds-button_neutral", onClick Decrement ] [ text "-" ]
                    , div [] [ text (String.fromInt model) ]
                    , button [ class "slds-button slds-button_neutral", onClick Increment ] [ text "+" ]
                    , button [ class "slds-button slds-button_neutral", onClick Reset ] [ text "RESETx" ]
                    ]
                ]
            ]
        ]

view : Model -> Html Msg
view model =
       Grid.container []         -- Responsive fixed width container
        [ CDN.stylesheet      -- Inlined Bootstrap CSS for use with reactor
        , navbar model        -- Interactive and responsive menu
        , mainContent model
        ]


viewLeftSidePane : Html msg
viewLeftSidePane =
    div
        [ attribute "aria-hidden" "false"
        , class "slds-panel slds-size_medium  slds-panel_docked slds-panel_docked-left slds-is-open"
        ]
        [ div [ class "slds-panel__header" ]
            [ h2 [ class "slds-panel__header-title slds-text-heading_small slds-truncate", title "Panel Header" ]
                [ text "Panel Header" ]
            , div [ class "slds-panel__header-actions" ]
                [ button [ class "slds-button slds-button_icon slds-button_icon-small slds-panel__close", title "Collapse Panel Header" ]
                    [ ---
                      span[ ariaHidden True] [text "✕"]
                    , span [ class "slds-assistive-text" ]
                        [ text "Collapse Panel Header" ]
                    ]
                ]
            ]
        , div [ class "slds-panel__body" ]
            [ text "A panel body accepts any layout or component" ]
        ]


viewHeader : Html msg
viewHeader =
    header [  ]
        -- class "slds-global-header_container"
        [ a [ class "slds-assistive-text slds-assistive-text_focus", href "#" ]
            [ text "Skip to Navigation" ]
        , a [ class "slds-assistive-text slds-assistive-text_focus", href "#" ]
            [ text "Skip to Main Content" ]
        , div [ class "slds-global-header slds-grid slds-grid_align-spread" ]
            [ div [ class "slds-global-header__item" ]
                [ div [ class "slds-global-header__logo" ]
                    [ span [ class "slds-assistive-text" ]
                        [ text "Salesforce" ]
                    ]
                ]
            , div [ class "slds-global-header__item slds-global-header__item_search" ]
                [ div [ class "slds-form-element" ]
                    [ label [ class "slds-form-element__label slds-assistive-text", for "combobox-id-2" ]
                        [ text "Search" ]
                    , div [ attribute "aria-expanded" "false", attribute "aria-haspopup" "listbox", class "slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click", id "primary-search-combobox-id-1", attribute "role" "combobox" ]
                        [ div [ class "slds-combobox__form-element slds-input-has-icon slds-input-has-icon_left slds-global-search__form-element", attribute "role" "none" ]
                            [ span [ class "slds-icon_container slds-icon-utility-search slds-input__icon slds-input__icon_left" ]
                                [ svg
                                    [ attribute "aria-hidden" "true"
                                    , SA.class "slds-icon slds-icon slds-icon_xx-small slds-icon-text-default"
                                    , SA.viewBox " 0 0 52 52"
                                    ]
                                    [ path
                                        [ SA.d "M49.598 45.298l-13.4-13.3c2.7-3.8 4.1-8.6 3.4-13.7-1.2-8.6-8.2-15.4-16.9-16.2-11.8-1.2-21.8 8.8-20.6 20.7.8 8.6 7.6 15.7 16.2 16.9 5.1.7 9.9-.7 13.7-3.4l13.3 13.3c.6.6 1.5.6 2.1 0l2.1-2.1c.6-.6.6-1.6.1-2.2zm-41.6-24.4c0-7.1 5.8-12.9 12.9-12.9 7.1 0 12.9 5.8 12.9 12.9 0 7.1-5.8 12.9-12.9 12.9-7.1 0-12.9-5.7-12.9-12.9z"
                                        , SA.fillRule "evenodd"
                                        ]
                                        []

                                    {- node "use"
                                       [ attribute "xlink:href" "/assets/icons/utility-sprite/svg/symbols.svg#search" ]
                                       []
                                    -}
                                    ]
                                ]
                            , input [ attribute "aria-autocomplete" "list", attribute "aria-controls" "search-listbox-id-1", attribute "autocomplete" "off", class "slds-input slds-combobox__input", id "combobox-id-2", placeholder "Search", attribute "role" "textbox", type_ "text" ]
                                []
                            , text ""
                            ]
                        ]
                    ]
                ]
            , div [ class "slds-global-header__item" ]
                [ ul [ class "slds-global-actions" ]
                    [ li [ class "slds-global-actions__item" ]
                        [ div [ class "slds-dropdown-trigger slds-dropdown-trigger_click" ]
                            [ button [ attribute "aria-haspopup" "true", class "slds-button slds-global-actions__avatar slds-global-actions__item-action", title "person name" ]
                                [ span [ class "slds-avatar slds-avatar_circle slds-avatar_medium" ]
                                    [ img [ alt "Person name", src "/assets/images/avatar2.jpg", title "User avatar" ]
                                        []
                                    , text ""
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
