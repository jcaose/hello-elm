module.exports = {
  // https://tailwindcss.com/docs/optimizing-for-production
  purge: {
    content: [
    './src/**/*.elm'
    ],
    options: {
      safelist: [
        /data-theme$/,
      ]
    }
  }
  ,
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: require('daisyui/colors'),
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('daisyui')
  ],
}
